﻿using System;

namespace TestWebApi.Models
{
    public class Client
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public String LastName { get; set; }
        public String Address { get; set; }
        public String City { get; set; }
        public DateTime Birthday { get; set; }
    }
}