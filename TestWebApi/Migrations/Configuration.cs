namespace TestWebApi.Migrations
{
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<TestWebApi.Models.TestWebApiContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(TestWebApi.Models.TestWebApiContext context)
        {
            context.Client.AddOrUpdate(
              p => p.Name,
              new Client { Name = "Andrew", LastName = "Peters", Address = "Street #1", Birthday = new DateTime(1980, 02, 15), City = "Santiago" },
              new Client { Name = "Brice", LastName = "Lambson", Address = "Street #2", Birthday = new DateTime(1975, 10, 04), City = "Sevilla" },
              new Client { Name = "Rowan", LastName = "Miller", Address = "Street #3", Birthday = new DateTime(1986, 08, 25), City = "Lima" }
            );
        }
    }
}
